﻿/*Author: David Hoffmann
 * Description: This script runs the game/dibs manager
 * Date Created: 4/20/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Added namespaces
using System;
using UnityEngine.AI;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DIBsManager : MonoBehaviour 
{
	//Makes this object a static and instantiates it to null to make into a singleton
	public static DIBsManager instance = null;

	//Creates a camera variable
	private GameObject bookCamera;

	//Creates a scene variable
	public Scene curScene;

	private GameObject bookPage;

	private GameObject transPanel;

	private GameObject standCamera;

	public bool TheNailSet = false;

	private bool fadeOut = false;

	private bool fadeIn = false;

	public GameObject customerSpawner;

	public GameObject customerPresent;

	public GameObject bookCover;

	public GameObject[] storyButtons;

	public float setTimer;

	private float bookOpenTimer;

	public GameObject bookPageTurn;

	private GameObject theMerch;

	//Creates a dictionary that stores states
	private Dictionary<GMStates, Action> GMSM = new Dictionary<GMStates, Action> ();

	//Creates an enum
	public enum GMStates
	{
		//The declared list of states
		MAINMENU,
		TRANSITION,
		THENAIL_SCENE_ONE,
		THENAIL_SCENE_TWO,
		THENAIL_SCENE_THREE,
		THENAIL_SCENE_FOUR,
		THENAIL_SCENE_FIVE,
		THENAIL_SCENE_SIX,
		THENAIL_SCENE_SEVEN,
		THEHAHE,
		THELILLAFI,
		THEMOBISA,
		THEOLWOWO,
		NUM_STATES
	}

	//Creates a GMStates variable
	public GMStates curState;



	//Use this for when the application starts
	void Awake ()
	{
		
		//Set of if/else if statements that ensure that this is the only game object of this type
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
		{
			Destroy (gameObject);
		}

		//Set the transform of this object
		transform.position = new Vector3(1000, 1000, 0);

		//Prevents this object from getting destroyed on scene load
		DontDestroyOnLoad (this);

		//Checks for duplicate DIBsManager objects
		if (FindObjectsOfType(GetType()).Length > 1)
		{
			//Destroy gameobject
			Destroy (gameObject);
		}
	}

	// Use this for initialization
	void Start () 
	{
		//Populates the dictionary with entries
		GMSM.Add (GMStates.MAINMENU, new Action(MainMenuState)); 
		GMSM.Add (GMStates.TRANSITION, new Action(TransitionState));
		GMSM.Add (GMStates.THENAIL_SCENE_ONE, new Action(TheNailSceneOneState));
		GMSM.Add (GMStates.THENAIL_SCENE_TWO, new Action(TheNailSceneTwoState));
		GMSM.Add (GMStates.THENAIL_SCENE_THREE, new Action(TheNailSceneThreeState));
		GMSM.Add (GMStates.THENAIL_SCENE_FOUR, new Action(TheNailSceneFourState));
		GMSM.Add (GMStates.THENAIL_SCENE_FIVE, new Action(TheNailSceneFiveState));
		GMSM.Add (GMStates.THENAIL_SCENE_SIX, new Action(TheNailSceneSixState));
		GMSM.Add (GMStates.THENAIL_SCENE_SEVEN, new Action(TheNailSceneSevenState));


		//Puts the active scene into curscene
		curScene = SceneManager.GetActiveScene ();

		//Set of if/else if statements that set the state for manager
		if (curScene.name == "MainMenu")
		{
			//Sets the variable's value
			bookCamera = GameObject.Find("Book Camera");

			//Set the state
			SetState(GMStates.MAINMENU);
		}



	}
	
	// Update is called once per frame
	void Update () 
	{
		//Invokes the curstate variable once per frame
		GMSM [curState].Invoke ();
	}

	public void SetTheNail ()
	{
		TheNailSet = true;
	}

	//Main Menu State
	void MainMenuState ()
	{
		if (curScene.name != "MainMenu")
		{
			//Set the next scene
			SceneManager.LoadScene("MainMenu");
		}
			
		if (bookCover.GetComponent<BookOpen>().beenClicked == true)
		{
			bookOpenTimer += Time.deltaTime * 1;
			
			if (bookOpenTimer >= setTimer)
			{
				storyButtons [0].SetActive (true);
			}
		}

		if (bookPageTurn.GetComponent<BookOpen>().beenClicked == true)
		{
			SetTheNail ();
			storyButtons [0].SetActive (false);
		}

		if (TheNailSet == true)
		{
			//Sets the bool to true so that the camera's zoom in starts
			bookCamera.GetComponent <Animator> ().SetBool ("ZoomIn", true);

			//Checks if the fieldOfview variable is equal to 8
			if (bookCamera.GetComponent<Camera> ().fieldOfView == 8)
			{
				//Set the next scene
				SceneManager.LoadScene ("TNaPage1");

				fadeOut = true;

				SetState (GMStates.TRANSITION);
			}
		}
	}

	//The Transition State
	void TransitionState ()
	{
		//Puts the active scene into curscene
		curScene = SceneManager.GetActiveScene ();

		if (curScene.name == "TNaPage1")
		{
			//Sets the variable's value
			standCamera = GameObject.Find("MarketZoomCam");

			//Sets the variable's value
			transPanel = GameObject.Find("TransPanel");

		

			//Set the state
			SetState(GMStates.THENAIL_SCENE_ONE);
		}

		if (fadeOut == true)
		{
			transPanel.GetComponent<Animator> ().SetBool ("FadeOut", true);

			transPanel.GetComponent<Animator> ().SetBool ("FadeIn", false);

			fadeIn = false;
		}

		if (fadeIn == true)
		{
			transPanel.GetComponent<Animator> ().SetBool ("FadeIn", true);

			transPanel.GetComponent<Animator> ().SetBool ("FadeOut", false);

			fadeOut = false;
		}

	}

	/*The Nail Scenes States
	 * This area is used for The Nail story line and its interactions
	*/
	void TheNailSceneOneState ()
	{
		//Sets the variable's value
		customerSpawner = GameObject.Find ("CustomerSpawner");

		customerPresent = GameObject.FindGameObjectWithTag ("Customer");

		theMerch = GameObject.FindGameObjectWithTag ("Merchant");

		if (curScene.name != "TNaPage1")
		{
			//Set the next scene
			SceneManager.LoadScene("TNaPage1");
		}

		if (customerPresent == null && theMerch.GetComponent<Merchant>().beenClicked == false)
		{
			customerSpawner.GetComponent<CustomerSpawner> ().setSpawn = true;
		}
	}
	void TheNailSceneTwoState ()
	{

	}
	void TheNailSceneThreeState ()
	{

	}
	void TheNailSceneFourState ()
	{

	}
	void TheNailSceneFiveState ()
	{

	}
	void TheNailSceneSixState ()
	{

	}
	void TheNailSceneSevenState ()
	{

	}



	//Custom function that sets the states
	public void SetState (GMStates newState)
	{
		//Set the curstate
		curState = newState;
	}
}
