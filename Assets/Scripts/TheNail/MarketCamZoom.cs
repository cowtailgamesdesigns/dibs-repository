﻿/*Author: David Hoffmann
 * Description: This script modifies the MarketCamZoom's fieldOfView variable through MechAnim to create a zoom-in effect 
 * Date Created: 3/30/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketCamZoom : MonoBehaviour 
{

	//Creates a variable for a GameObject to be assigned in the inspector window
	public GameObject marketStaticCam;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Checks to see if the fieldOfView variable is equal to 15
		if (GetComponent<Camera> ().fieldOfView == 15)
		{
			//Activate the marketStaticCam object
			marketStaticCam.SetActive(true);

			//Deactivate this gameObject
			gameObject.SetActive (false);
		}
	}
}
