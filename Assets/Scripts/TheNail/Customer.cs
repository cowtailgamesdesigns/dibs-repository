﻿/*Author: David Hoffmann
 * Description: This script runs the customer character's finite state machine
 * Date Created: 4/19/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Added namespaces to access the Action event and NavMeshAgent component
using UnityEngine.AI;
using System;

public class Customer : MonoBehaviour 
{
	//Creates an enum variable for states
	public enum CustStates
	{
		//Creates the Idle enum
		IDLE,

		//Creates the Walking enum
		WALKING,

		//Creates the Num_States enum
		NUM_STATES
	}

	//Makes this object a static and instantiates it to null to make into a singleton
	public static Customer instance = null;

	//Creates a Dictionary variable that stores the enum and calls the action event
	private Dictionary<CustStates, Action> FSM = new Dictionary<CustStates, Action>();

	//Creates an enum variable
	private CustStates curState;

	//Creates a NavMeshAgent variable
	private NavMeshAgent myAgent;

	//Creates a private GameObject variable
	private GameObject counterWP;

	//Creates a private GameObject variable
	private GameObject endWP;

	//Creates a float variable that can be accessed by other objects
	public float testRunningTimer;

	//Creates a float variable that can be accessed by other objects
	public float checkTimer = 3;

	//Creates an integer variable
	private int headToEnd = 0;

	//Creates a gameobject variable
	private GameObject theMerch;

	// Use this for initialization
	void Start ()
	{
		//Set of if/else if statements that ensure that this is the only game object of this type
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null)
		{
			Destroy (gameObject);
		}

		//Stores the NavMeshAgent component
		myAgent = GetComponent <NavMeshAgent> ();

		//Adds dictionary entries
		FSM.Add (CustStates.IDLE, new Action(CustomerIdle)); 
		FSM.Add (CustStates.WALKING, new Action(CustomerWalking));

		//Set the GameObject variable
		counterWP = GameObject.FindGameObjectWithTag ("CustCoutWP");

		//Set the GameObject variable
		endWP = GameObject.FindGameObjectWithTag ("CustEndWP");

		theMerch = GameObject.FindGameObjectWithTag ("Merchant");

		//Pass the state into the function
		SetState (CustStates.WALKING);

	}
		
	
	// Update is called once per frame
	void Update () 
	{
		//Invoke the state machine each frame
		FSM [curState].Invoke ();
	}

	//Custom function for the idle state
	void CustomerIdle ()
	{
		
		if (theMerch.GetComponent<Merchant>().beenClicked == true)
		{
			//Set the variable to 1
			headToEnd = 1;

			//Pass the state into the function 
			SetState (CustStates.WALKING);
		}
	}

	//Custom function for the walking state
	void CustomerWalking ()
	{
		//Checks if headToEnd variable equals zero
		if (headToEnd == 0)
		{
			//Set the NavMeshAgent's destination to the merchant's stand waypoint
			myAgent.destination = counterWP.GetComponent<Transform> ().position;
		}

		//Checks if headToEnd variable equals one
		if (headToEnd == 1)
		{
			//Sets the NavMeshAgent's destination to the end waypoint
			myAgent.destination = endWP.GetComponent<Transform> ().position;
		}
			
	}

	//Unity's function that checks if this collider enters another
	void OnTriggerEnter (Collider other)
	{
		//Checks if the other variable equals the string
		if (other.tag == "CustCoutWP")
		{
			//Pass the state into the function
			SetState (CustStates.IDLE);
		}

		//Checks if the other variable equals the string
		if (other.tag == "CustEndWP")
		{
			//Destroy this object
			Destroy (gameObject);
		}
	}

	//Custom function that accepts state values
	public void SetState (CustStates newState)
	{
		//Set the variable to the value
		curState = newState;
	}
}
