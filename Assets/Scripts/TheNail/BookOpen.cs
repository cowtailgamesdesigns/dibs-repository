﻿/*Author: David Hoffmann
 * Description: This script runs the book open/close animations
 * Date Created: 3/30/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Added namespace to access the Action event
using System;

public class BookOpen : MonoBehaviour 
{
	//Creates a Dictionary variable
	private Dictionary<BookState, Action> BSM = new Dictionary<BookState, Action> ();

	//Creates an enum that is accessible by other objects
	public enum BookState
	{
		//Closed idle state
		IDLE_CLOSED,

		//Open idle state
		IDLE_OPEN,

		//The number of states
		NUM_STATES
	}

	//Creats a variable that is accessible by other objects
	public BookState curState = BookState.IDLE_CLOSED;

	//Creates a bool variable that is accessible by other objects
	public bool beenClicked;

	//Creates an Animator variable
	private Animator anim;

	public GameObject[] storyButtons;

	// Use this for initialization
	void Start () 
	{
		//Stores the animator component
		anim = GetComponent<Animator> ();

		//Sets the Open parameter to false
		anim.SetBool ("Open", false);

		//Sets the Close parameter to false
		anim.SetBool("Close", false);

		//Adds an entry to the dictionary
		BSM.Add (BookState.IDLE_CLOSED, new Action(ClosedBook));

		//Adds an entry to the dictionary
		BSM.Add (BookState.IDLE_OPEN, new Action(OpenedBook));

		//Sets the beenClicked variable to false
		beenClicked = false;

		//Passes a BookState enum into the SetState function
		SetState (BookState.IDLE_CLOSED);
	}
	
	// Update is called once per frame
	void Update () 
	{
		//Runs the BSM Dictionary actions every frame
		BSM [curState].Invoke ();
	}

	//Function that runs when left mouse button has been pressed
	void OnMouseDown ()
	{
		//Checks if beenClicked variable is true
		if (beenClicked == true && gameObject.tag == "BookCover")
		{
			//Passes a BookState enum into the SetState function
			SetState (BookState.IDLE_CLOSED);

			//Sets the beenClicked variable to false
			beenClicked = false;
		}
		//Checks if beenClicked equals false
		else if (beenClicked == false && gameObject.tag == "BookCover")
		{
			//Passes a BookState enum into the SetState function
			SetState (BookState.IDLE_OPEN);

			//Sets the beenClicked variable to true
			beenClicked = true;
		}
	}

	//Custom function to run the close animation
	void ClosedBook ()
	{
		//Set the Close parameter to true
		anim.SetBool ("Close", true);

		//Set the Open parameter to false
		anim.SetBool("Open", false);
	}

	//Custom function to run the open animation
	public void OpenedBook ()
	{
		//Set the Open parameter to true
		anim.SetBool ("Open", true);

		//Set the Close parameter to false
		anim.SetBool ("Close", false);


	}

	public void openThePage()
	{
		//Passes a BookState enum into the SetState function
		SetState (BookState.IDLE_OPEN);

		beenClicked = true;
	}

	//Custom function that allows passing states and sets it to the curState variable
	public void SetState (BookState newState)
	{
		//Sets the curState variable
		curState = newState;
	}
}
