﻿/*Author: David Hoffmann
 * Description: This script modifies the BookCamZoom's fieldOfView variable through MechAnim to create a zoom-in effect
 * Date Created: 3/30/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BookCamZoom : MonoBehaviour 
{
	//Creates a variable to hold a gameobject through the inspector tab
	public GameObject bookPage;

	//Creates a variable to hold an Animator component
	public Animator anim;

	//Creates a variable to hold the game manager object
	public GameObject managerPresent;

	public GameObject theManager;

	// Use this for initialization
	void Start () 
	{
		//Checks for the manager game object
		managerPresent = GameObject.FindGameObjectWithTag ("DIBsManager");

		//Checks if the variable is empty
		if (managerPresent == null)
		{
			//Creates a game manager object
			Instantiate (theManager, transform.position, transform.rotation);
		}

		//Stores the animator component
		anim = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		/*//Checks if the beenClicked parameter is true
		if (bookPage.GetComponent<BookOpen> ().beenClicked == true)
		{
			//Set the ZoomIn parameter to true
			anim.SetBool ("ZoomIn", true);
		}*/

	}
}
