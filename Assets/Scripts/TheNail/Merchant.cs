﻿/*Author: David Hoffmann
 * Description: This script is the finite state machine for the merchant character
 * Date Created: 4/19/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Added namespaces to access the Action event and NavMeshAgent component
using System;
using UnityEngine.AI;

public class Merchant : MonoBehaviour 
{
	//Creates an enum variable for states
	public enum MerchStates
	{
		//Creates the selling enum
		SELLING,

		//Creates the deposit enum
		DEPOSIT,

		//Creates the return enum
		RETURN_TO_COUNTER,

		//Creates the walking enum
		WALK_TO_MONEY,

		//Creates the close shop enum
		CLOSE_SHOP,

		//Creates the move to horse enum
		WALK_TO_HORSE,

		//Creats the mount horse enum
		MOUNT_HORSE,

		//Creates the Num_States enum
		NUM_STATES
	}

	//Creates a Dictionary variable that stores the enum and calls the action event
	private Dictionary<MerchStates, Action> MSM = new Dictionary<MerchStates, Action>();

	//Creates an enum variable
	public MerchStates curState;

	//Creates a NavMeshAgent variable
	private NavMeshAgent myAgent;

	//Creates a private GameObject variable
	private GameObject merchCounterWP;

	//Creates a private GameObject variable
	private GameObject merchGoldWP;

	[SerializeField]
	//Creates a bool variable
	public bool beenClicked;

	[SerializeField]
	//Creates a bool variable
	private bool beenClickedAgain;

	//Creates a gameobject variable
	private GameObject presentCustomer;

	//Creates a gameobject variable
	private GameObject customerWP;

	//Creates a gameobject variable
	private GameObject horseStageWP;

	//Creates a gameobject variable
	private GameObject horseSaddle;

	//Creates a float variable
	public float speed;

	//Creates a game object variable
	public CustomerPresent custCounterWayPoint;

	// Use this for initialization
	void Start ()
	{
		//Stores the NavMeshAgent component
		myAgent = GetComponent <NavMeshAgent> ();

		//Adds dictionary entries
		MSM.Add (MerchStates.SELLING, new Action(MerchSales)); 
		MSM.Add (MerchStates.WALK_TO_MONEY, new Action(MerchWalkToMoneyBag));
		MSM.Add (MerchStates.DEPOSIT, new Action(DepositMoney)); 
		MSM.Add (MerchStates.RETURN_TO_COUNTER, new Action(BackToCounter));
		MSM.Add (MerchStates.CLOSE_SHOP, new Action(CloseShop));
		MSM.Add (MerchStates.WALK_TO_HORSE, new Action(WalkToHorse)); 
		MSM.Add (MerchStates.MOUNT_HORSE, new Action(MountHorse));

		//Set the GameObject variable
		merchCounterWP = GameObject.FindGameObjectWithTag ("MerchCounWP");

		//Set the GameObject variable
		merchGoldWP = GameObject.FindGameObjectWithTag ("MerchGoldWP");

		//Set the state to idle
		SetState (MerchStates.SELLING);

		//Disable this object's collider component
		GetComponent<Collider>().enabled = false;

		//Set the variable to false
		beenClicked = false;

		//Set the variable to false
		beenClickedAgain = false;

		//Set this object's position to the merchant's counter waypoint
		GetComponent<Transform> ().position = merchCounterWP.GetComponent<Transform> ().position;

		//Set the state to merchant selling
		SetState (MerchStates.SELLING);

	}

	// Update is called once per frame
	void Update () 
	{
		//Invoke the state machine each frame
		MSM [curState].Invoke ();
	}

	//Custom function for the deposit state
	void DepositMoney ()
	{
		//Enable this object's collider component
		GetComponent<Collider>().enabled = true;
	}

	//Custom function for the idle state
	void MerchSales ()
	{

		if (custCounterWayPoint.customerPresent == true)
		{
			//Enable this object's collider component
			GetComponent<Collider>().enabled = true;
		}


	}

	//Custom function for the walking state
	void MerchWalkToMoneyBag()
	{
		//Set the NavMeshAgent's destination to the gold waypoint
		myAgent.destination = merchGoldWP.GetComponent<Transform>().position;

		//Disable this object's collider component
		GetComponent<Collider>().enabled = false;

		//If statement that checks if this position is same as destination
		if (gameObject.transform.position == merchGoldWP.GetComponent<Transform> ().transform.position)
		{
			//Set the state to deposit
			SetState (MerchStates.DEPOSIT);
		}
	}

	//Custom function for the return to counter state
	void BackToCounter()
	{
		
		//Set the NavMeshAgent's destination to the gold waypoint
		myAgent.destination = merchCounterWP.GetComponent<Transform>().position;

		//Disable this object's collider component
		GetComponent<Collider>().enabled = false;

		//If statement that checks if this position is same as destination
		if (gameObject.transform.position == merchCounterWP.GetComponent<Transform> ().transform.position)
		{
			//Checks if both variables are true and sets them to false
			if (beenClicked == true && beenClickedAgain == true)
			{
				//Sets the variable to false
				beenClicked = false;
				//Sets the variable to false
				beenClickedAgain = false;
			}

			SetState (MerchStates.SELLING);
		}
			
	}

	//Custom function that handles the close shop state
	void CloseShop()
	{
		
	}

	//Custom function that handles the walk to horse state
	void WalkToHorse()
	{
		
	}

	//Custom function that handles the mount horse state
	void MountHorse()
	{
		//Set the speed to increment
		speed += Time.deltaTime * 1;

		//Checks if this objects position doesn't equal the target's position
		if (transform.position != horseSaddle.GetComponent<Transform>().position)
		{
			//Translate the object to the saddle's position
			transform.position = Vector3.MoveTowards(transform.position, horseSaddle.GetComponent<Transform>().position, speed);
		}
		else
		{
			//Keep the merchants position the same as the saddle's position
			transform.position = horseSaddle.GetComponent<Transform> ().position;	
		}
	}

	//Unity's function that checks if this collider enters another
	void OnTriggerEnter (Collider other)
	{
		if (other.tag == "MerchGoldWP")
		{
			GetComponent<Collider> ().enabled = true;
			SetState (MerchStates.DEPOSIT);
		}
	}

	//Unity's function for when an object gets clicked
	void OnMouseDown ()
	{
		//If/else if statements that mamages when this object has been clicked 
		if (beenClicked == false && curState == MerchStates.SELLING)
		{
			beenClicked = true;
			SetState (MerchStates.WALK_TO_MONEY);
		}
		else if (beenClickedAgain == false && curState == MerchStates.DEPOSIT)
		{
			beenClickedAgain = true;
			SetState (MerchStates.RETURN_TO_COUNTER);
		}


	}

	//Custom function that accepts state values
	public void SetState (MerchStates newState)
	{
		//Set the variable to the value
		curState = newState;
	}
}
