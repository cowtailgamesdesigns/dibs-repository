﻿/*Author: David Hoffmann
 * Description: This script is the finite state machine for the merchant character
 * Date Created: 4/20/2017
 * Last Modified: 4/20/2017
*/

//Standard used namespaces
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Added namespaces to access the Action event
using System;

public class CustomerSpawner : MonoBehaviour 
{
	//Creates an enum to assign values to the state variables that can be accessed by other objects
	public enum SpawnState
	{
		//Creates a spawn one variable
		SPAWN_ONE,

		//Creates an idle variable
		IDLE,

		//Creates a variable that defines the number of states
		NUM_STATES
	}

	//Creates a private dictionary
	private Dictionary<SpawnState, Action> SSM = new Dictionary<SpawnState, Action> ();

	//Creates a private variable to hold enum states
	private SpawnState curState;

	//Creates a variable that can be accessed by other objects
	public bool setSpawn;

	//Creates a GameObject variable for the Customer object to be assigned in the inspector tab
	[SerializeField]
	private GameObject customerObject;

	//Creates a float variable
	[Tooltip("Set the delay timer for the spawner.")]
	public float spawnTimerCheck = 3.0f;

	//Creates a float variable
	[SerializeField]
	private float spawnDelayTimer;

	//Creates a GameObject variable assigned in the inspector tab
	public GameObject staticMarketCam;

	//Use this for initialization
	void Start () 
	{
		//Populates the dictionary
		SSM.Add (SpawnState.SPAWN_ONE, new Action(spawnCustomer));
		SSM.Add (SpawnState.IDLE, new Action(spawnIdle));

		//Checks if the variable has a value
		if (!staticMarketCam)
		{
			//Send an error message to the debug window
			Debug.Log("The stand camera is missing! Assign it to the inspector!");
		}

		//Set the state to idle
		SetState(SpawnState.IDLE);
	}

	//Update is called once per frame
	void Update () 
	{
		//Invoke the state machine once per frame
		SSM [curState].Invoke ();
	}

	//Custom function that runs the customer spawning state
	void spawnCustomer ()
	{
		//Spawn the customer
		Instantiate (customerObject, transform.position, transform.rotation);
	}

	//Custom function that runs the idle state
	void spawnIdle ()
	{
		
		//Checks if the setSpawn variable is true
		if (setSpawn == true)
		{
			//Set the state to spawn one
			SetState (SpawnState.SPAWN_ONE);
		}

	}

	//Unity's function to check a collider enter
	void OnTriggerEnter (Collider other)
	{
		//Check if collider's tag is customer and the setSpawn variable is true
		if (other.tag == "Customer" && setSpawn == true)
		{
			//Set the bool variable to false
			setSpawn = false;

			//Set the state to idle
			SetState (SpawnState.IDLE);
		}
	}

	//Custom function that accepts states and can be accessed by other objects
	public void SetState (SpawnState newState)
	{
		//Set the curState variable
		curState = newState;
	}
}
