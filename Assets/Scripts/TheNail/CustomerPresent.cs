﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerPresent : MonoBehaviour {

	public bool customerPresent = false;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Customer")
		{
			customerPresent = true;
		}

	}

	void OnTriggerExit (Collider other)
	{
		if (other.tag == "Customer")
		{
			customerPresent = false;
		}
	}
}
