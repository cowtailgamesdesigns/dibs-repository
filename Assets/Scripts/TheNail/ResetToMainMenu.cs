﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class ResetToMainMenu : MonoBehaviour 
{

	private DIBsManager theManager;

	// Use this for initialization
	void Start ()
	{
		theManager = GameObject.FindObjectOfType<DIBsManager> ();

		if (theManager == null)
		{
			SceneManager.LoadScene ("MainMenu");
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}
}
